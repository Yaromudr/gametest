//========================================================================================
//  
//  ADOBE CONFIDENTIAL
//   
//  $File: //depot/Genie/PrivateBranch/Piyush/GenieTool/ScriptInterface/src/com/adobe/genie/iexecutor/components/IGenieSprite.java $
// 
//  Owner: Ruchir Jain
//  
//  $Author: psinghal $
//  
//  $DateTime: 2012/03/30 14:22:17 $
//  
//  $Revision: #1 $
//  
//  $Change: 5952 $
//  
//  Copyright 2010 Adobe Systems Incorporated
//  All Rights Reserved.
//  
//  NOTICE:  All information contained herein is, and remains
//  the property of Adobe Systems Incorporated and its suppliers,
//  if any.  The intellectual and technical concepts contained
//  herein are proprietary to Adobe Systems Incorporated and its
//  suppliers and are protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material
//  is strictly forbidden unless prior written permission is obtained
//  from Adobe Systems Incorporated.
//  
//========================================================================================
package com.adobe.genie.iexecutor.components;

public interface IGenieSprite extends IGenieDisplayObject{

}
