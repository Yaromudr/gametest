//========================================================================================
//Copyright � 2012, Adobe Systems Incorporated
//All rights reserved.
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
//	documentation and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
//  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
//  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//========================================================================================
package com.adobe.geniePlugin.model;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Property {
	
		private String propertyKey;
		private String propertyValue;
		private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(
				this);


		public Property() {
		}

		public Property(String key, String value) {
			super();
			this.propertyKey = key;
			this.propertyValue = value;
		}

		public void addPropertyChangeListener(String propertyName,
				PropertyChangeListener listener) {
			propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
		}

		public void removePropertyChangeListener(PropertyChangeListener listener) {
			propertyChangeSupport.removePropertyChangeListener(listener);
		}

		public String getKey() {
			return propertyKey;
		}

		public String getValue() {
			return propertyValue;
		}

		public void setKey(String key) {
			propertyChangeSupport.firePropertyChange("key", this.propertyKey,
					this.propertyKey = key);
		}

		public void setValue(String value) {
			propertyChangeSupport.firePropertyChange("key", this.propertyValue,
					this.propertyValue = value);
		}

}
