package ru.goblin.gametest.pages;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.goblin.gametest.utilites.LightDriver;

import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;

/**
 * Created by Aleksandr on 02.12.2015.
 */
public class MainPage extends AbstractPage {

    private MainPage self = null;
    public static String basikLink;

    private String username = "testing101xp@rambler.ru";
    private String password = "1q2w3e4r5t";
    private List<String> serversList;


    @Override
    protected AbstractPage getThis() {
        return self;
    }

    public MainPage(LightDriver driver) {
        super(driver);
        self = this;
    }

    @Override
    public String getFirstItem() {
        return null;
    }

    public AbstractPage login() {
        String loginXpath = "//div[contains(text(), 'Войти')]";
        String userNameXpath = "//input[contains(@name, 'email')]";
        String passwordXpath = "//input[contains(@name, 'password')]";

        String loginXpathAfterInputLoginData = "//span[contains(text(), 'Войти')]";
        clickByCaption(loginXpath);
        setText(userNameXpath, username);
        setText(passwordXpath, password);
        clickByCaption(loginXpathAfterInputLoginData);

        wait.until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                String xpath = "//span[contains(@class,'display-name') and ancestor::div[contains(@class, 'user-panel')]]";
                List<WebElement> list = driver.findElements(By.xpath(xpath));
                return list.get(0).getText().equals("testing");
            }
        });
        return getThis();
    }

    public AbstractPage changeGame(String gamename) {
        String gameXpath = format("//div[contains(text(), '%s') and (preceding-sibling::div[contains(@class, 'icon')]) ]", gamename);
        Actions actions = new Actions(driver);
        String menuXpath = format("//li[contains(text(), '%s')]", "Игры");
        actions.moveToElement(driver.findElement(By.xpath(menuXpath))).build().perform();
        clickByCaption(gameXpath);
        return getThis();
    }

    public List<String> getServersList() {
        driver.getWait().until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                System.out.println("Загрузка страницы...");
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        });
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<WebElement> frames = driver.findElements(By.xpath("//iframe[ancestor::div[contains(@id, 'canvas')]]"));
        driver.switchTo().defaultContent();
        WebElement frame = frames.get(0);
        String link = frame.getAttribute("src");
        driver.get(link);
        String buf = link.substring(8, link.length());
        basikLink = "https://".concat(buf.substring(0, buf.indexOf("/")));
        String tableItemXpath = "//div[contains(@class, 'btn') and ancestor::div[contains(@class, 'server-list') and contains(@id, 'all-servers') and ancestor::div[contains(@class, 'all-servers')]]]";
        String serversLocator = format("//div[contains(text(),'%s') and contains(@class, 'btn-link')]", "Все серверы");
        clickByCaption(serversLocator);
        serversList = new LinkedList<>();

        driver.getWait().until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                List<WebElement> list = driver.findElements((By.xpath(tableItemXpath)));
                System.out.println("Поиск таблицы...");
                return !list.isEmpty();
            }
        });
        driver.findElements(By.xpath(tableItemXpath)).forEach(e -> serversList.add(e.getAttribute("data-href")));

        return serversList;
    }
}
