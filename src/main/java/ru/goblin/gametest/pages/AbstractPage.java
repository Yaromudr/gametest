package ru.goblin.gametest.pages;

import com.google.common.base.Predicate;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.goblin.gametest.utilites.LightDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;

/**
 * Created by Aleksandr on 02.12.2015.
 */
public abstract class AbstractPage<T extends AbstractPage<T>> {
    private static Logger LOGGER = Logger.getLogger(AbstractPage.class);

    protected LightDriver driver;
    protected WebDriverWait wait;

    protected abstract T getThis();


    public AbstractPage(LightDriver driver) {
        this.driver = driver;
        wait = driver.getWait();
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

    }

    /**
     * Метод поиска элементов по XPath
     * Иногда их несколько на странице
     *
     * @param xpath - локатор для поиска
     * @return возвращает новую страницу
     */
    private List<WebElement> findElementsByXPath(CharSequence xpath) {
        LOGGER.info((format("Поиск элемента на сайте по Xpath [%s]", xpath)));
        final By by = By.xpath(xpath.toString());
        wait.until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                LOGGER.info("Поиск элементов...");
                return !(driver.findElements(by).isEmpty());
            }
        });
        List<WebElement> elements = driver.findElements(by);
        return elements;
    }

    /**
     * Нажимает на ссылку по видимому тексту
     *
     * @param xpath видимый текст
     * @return возвращает новую страницу
     */
    public T clickByCaption(CharSequence xpath) {
        clicker(xpath);
        return getThis();
    }

    /**
     * Собственно нажималка
     *
     * @param xpath - локатор для поиска
     * @return возвращает новую страницу
     */
    private T clicker(CharSequence xpath) {
        List<WebElement> elements = findElementsByXPath(xpath);
        LOGGER.info(format("Найдено элементов [%s]", elements.size()));
        elements.get(0).click();
        return getThis();
    }

    /**
     * Выбирает CheckBox по видимому тексту рядом
     *
     * @param xpath - локатор для поиска
     * @return возвращает новую страницу
     */

    public T setCheckBox(CharSequence xpath) {
        LOGGER.info((format("Изменение состояния CheckBox по адресу [%s]", xpath)));
        return clicker(xpath);
    }

    /**
     * Устанавливает значение цены
     *
     * @param xpath - локатор для поиска
     * @param text  - цена
     * @return возвращает новую страницу
     */
    public T setText(String xpath, String text) {
        List<WebElement> elements = findElementsByXPath(xpath);
        LOGGER.info(format("Найдено элементов [%s]", elements.size()));
        elements.get(0).sendKeys(text);
        return getThis();
    }


    public abstract String getFirstItem();

    /**
     * Берет значение имени первого элемента в выдаче поиска по фильтрам
     *
     * @param xpath - локатор для поиска
     * @return возвращает новую страницу
     */
    public String getFirstItem(CharSequence xpath) {
        //не придумаю я как сюда waiter вкрутить на перезагрузку страницы
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.error("Ошибка ожидания загрузки страницы", e);
        }
        List<WebElement> elements = findElementsByXPath(xpath);
        String item = elements.get(0).getText();
        LOGGER.info(format("Считано значение [%s]", item));
        return item;
    }


    private String searchLineXpath = "//input[(contains(@name, 'text')) and (contains(@id, 'header-search'))]";

    /**
     * Осуществляет поисковый запрос. Локатор поля ввода выше
     *
     * @param text - текст запроса
     * @return возвращает новую страницу
     */
    public void search(CharSequence text) {
        List<WebElement> elements = findElementsByXPath(searchLineXpath);
        WebElement el = elements.get(0);
        el.sendKeys(text);
        el.submit();
    }

    /**
     * Нажатие на элемент Верхнего меню
     *
     * @param xpath - локатор для поиска
     * @return возвращает новую страницу
     */

    public T clickForMenu(String xpath) {
        List<WebElement> elements = findElementsByXPath(xpath);
        LOGGER.info(format("Найдено элементов [%s]", elements.size()));
        String dopLink = elements.get(0).getAttribute("href");
        driver.get(dopLink);
        return getThis();
    }

    /**
     * Нажатие на элемент Левого меню
     *
     * @param xpath - локатор для поиска
     * @return возвращает новую страницу
     */
    public T clickForLeftMenu(String xpath) {
        clickForMenu(xpath);
        return getThis();
    }
}
