package ru.goblin.gametest.utilites;

import org.apache.log4j.Logger;
import org.testng.Assert;

import static java.lang.String.format;

/**
 * Created by Aleksandr on 02.12.2015.
 */
public class AssertManager {
    private static Logger LOGGER = Logger.getLogger(AssertManager.class);

    //private static String xpathItem = "//div[contains(@class, 'snippet-card__table')]";

    public static void assertTitle(LightDriver driver, String expectedTitle) {
        LOGGER.info(format("Производится сравнение заголовков. Ожидаемое значение [%s]", expectedTitle));
        Assert.assertEquals(expectedTitle, driver.getTitle(), "Значение заголовка отличаеся от ожидаемого");
        LOGGER.info("Заголовок соответствует ожидаемому значению");
    }

  /*  public static void assertRecord(final LightDriver driver, WebDriverWait wait, int expectedTitle) {
        LOGGER.info(format("Производится подсчет отображаемых элементов. Ожидаемое значение [%s]", expectedTitle));

        final By by = By.xpath(xpathItem);
        wait.until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                LOGGER.info("Поиск элементов...");
                return !(driver.findElements(by).isEmpty());
            }
        });
        List<WebElement> element = driver.findElements(by);
        LOGGER.info(format("Найдено элементов [%s]", element.size()));
        Assert.assertEquals(expectedTitle, element.size(), "Число элементов отличаеся от ожидаемого");
        LOGGER.info("Число элементов соответствует ожидаемому значению");
    }

    private static String xpathItemFromPage = "//h1[contains(@class, 'title') and (ancestor::*[contains(@class, 'headline__header')])]";

    public static void assertNames(final LightDriver driver, WebDriverWait wait, String expectedTitle) {
        LOGGER.info(format("Производится сравнение элементов. Ожидаемое значение [%s]", expectedTitle));

        final By by = By.xpath(xpathItemFromPage);
        wait.until(new Predicate<WebDriver>() {
            public boolean apply(WebDriver webDriver) {
                LOGGER.info("Поиск элементов...");
                return !(driver.findElements(by).isEmpty());
            }
        });
        List<WebElement> element = driver.findElements(by);
        LOGGER.info(format("Найдено элементов [%s]", element.size()));
        Assert.assertEquals(expectedTitle, element.get(0).getText(), "Текст считанного элемента отличаеся от ожидаемого");
        LOGGER.info("Текст считанного элемента соответствует ожидаемому значению");
    }*/
}
