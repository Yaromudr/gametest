package ru.goblin.gametest.utilites;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Aleksandr on 03.12.2015.
 */
public class LightDriver extends FirefoxDriver {

    private WebDriverWait wait;

    public LightDriver() {
        wait = new WebDriverWait(this, 10);
    }

    public WebDriverWait getWait() {
        return wait;
    }
}
