import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.goblin.gametest.pages.MainPage;
import ru.goblin.gametest.utilites.AssertManager;

import java.util.List;

/**
 * Created by Aleksandr on 20.12.2015.
 */
public class TestStartPage extends BasicTest {
    @BeforeTest
    public static void initDriver() {
        init();
    }

    private String urlHost = "http://101xp.com/";
    private MainPage mainPage;

    @Test
    public void login() {
        driver.get(urlHost);
        AssertManager.assertTitle(driver, "101XP - Играй бесплатно в онлайн-игры");
        mainPage = new MainPage(driver);
        mainPage.login();
        mainPage.changeGame("Dragon Knight");
        List<String> servers = mainPage.getServersList();
        driver.get(MainPage.basikLink.concat(servers.get(0)));

        System.out.println();

    }

    @AfterTest
    public void afterTest() {
        after();
    }

}
