import ru.goblin.gametest.pages.AbstractPage;
import ru.goblin.gametest.utilites.LightDriver;

/**
 * Created by Aleksandr on 02.12.2015.
 */
public abstract class BasicTest {
    protected static LightDriver driver;



    protected static void init() {
        driver = new LightDriver();
        driver.manage().window().maximize();
    }

    protected static void after() {
        driver.close();
    }
}
